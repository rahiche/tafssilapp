program App;

uses
  Vcl.Forms,
  HomeScreen in 'screens\HomeScreen.pas' {Form1},
  Theme in 'Theme.pas' {DataModule1: TDataModule},
  DataEntry in 'screens\DataEntry.pas' {DataEntryFrm},
  OperationsScreen in 'screens\OperationsScreen.pas' {OperationsFrm},
  TafssilScreen in 'screens\TafssilScreen.pas' {tafssilFrm},
  LabsScreen in 'screens\LabsScreen.pas' {frm_labs},
  TailorsScreen in 'screens\TailorsScreen.pas' {frm_tailors},
  ItemsScreen in 'screens\ItemsScreen.pas' {frm_items},
  ClientsScreen in 'screens\ClientsScreen.pas' {frm_clients},
  SupplierisScreen in 'screens\SupplierisScreen.pas' {frm_suppliers},
  SizesScreen in 'screens\SizesScreen.pas' {frm_sizes},
  Database in 'shared_data\Database.pas' {Databse: TDataModule},
  backup in 'screens\backup.pas' {frmbackup};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TDataModule1, DataModule1);
  Application.CreateForm(TDataEntryFrm, DataEntryFrm);
  Application.CreateForm(TOperationsFrm, OperationsFrm);
  Application.CreateForm(TtafssilFrm, tafssilFrm);
  Application.CreateForm(Tfrm_labs, frm_labs);
  Application.CreateForm(Tfrm_tailors, frm_tailors);
  Application.CreateForm(Tfrm_items, frm_items);
  Application.CreateForm(Tfrm_clients, frm_clients);
  Application.CreateForm(Tfrm_suppliers, frm_suppliers);
  Application.CreateForm(Tfrm_sizes, frm_sizes);
  Application.CreateForm(TDatabse, Databse);
  Application.CreateForm(Tfrmbackup, frmbackup);
  Application.Run;
end.
