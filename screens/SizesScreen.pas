﻿unit SizesScreen;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog, Data.DB,
  cxDBData, Vcl.Menus, cxContainer, cxTextEdit, Vcl.StdCtrls, cxButtons,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ExtCtrls, Vcl.Mask,
  Vcl.DBCtrls, cxGroupBox, Vcl.Grids, Vcl.DBGrids, cxMaskEdit, cxSpinEdit,
  cxDBEdit;

type
  Tfrm_sizes = class(TForm)
    DisplayPnl: TPanel;
    HeaderPnl: TPanel;
    Label1: TLabel;
    NewCar: TcxButton;
    DeleteCar: TcxButton;
    EditCar: TcxButton;
    cxButton7: TcxButton;
    DataPnl: TcxGroupBox;
    DBGrid1: TDBGrid;
    Panel11: TPanel;
    GridPanel1: TGridPanel;
    Panel1: TPanel;
    Label2: TLabel;
    cxDBSpinEdit1: TcxDBSpinEdit;
    Panel2: TPanel;
    Label3: TLabel;
    cxDBSpinEdit2: TcxDBSpinEdit;
    Panel3: TPanel;
    Label4: TLabel;
    cxDBSpinEdit3: TcxDBSpinEdit;
    Panel4: TPanel;
    Label5: TLabel;
    cxDBSpinEdit4: TcxDBSpinEdit;
    Panel5: TPanel;
    Label6: TLabel;
    cxDBSpinEdit5: TcxDBSpinEdit;
    Panel6: TPanel;
    Label7: TLabel;
    cxDBSpinEdit6: TcxDBSpinEdit;
    Panel7: TPanel;
    Label8: TLabel;
    cxDBSpinEdit7: TcxDBSpinEdit;
    Panel8: TPanel;
    Label9: TLabel;
    cxDBSpinEdit8: TcxDBSpinEdit;
    Panel9: TPanel;
    Label10: TLabel;
    cxDBSpinEdit9: TcxDBSpinEdit;
    Panel10: TPanel;
    Label11: TLabel;
    cxDBSpinEdit10: TcxDBSpinEdit;
    procedure cxButton7Click(Sender: TObject);
    procedure DeleteCarClick(Sender: TObject);
    procedure EditCarClick(Sender: TObject);
    procedure NewCarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_sizes: Tfrm_sizes;

implementation

{$R *.dfm}

uses DataEntry, Database;

procedure Tfrm_sizes.cxButton7Click(Sender: TObject);
begin
  close;
  with DataEntryFrm do
  begin
    Databse.tblSize.close();
    DisplayPnl.Align := alBottom;
    DisplayPnl.hide;
    GridPanel1.show;
  end;
end;

procedure Tfrm_sizes.DeleteCarClick(Sender: TObject);
begin
  if (not DBGrid1.DataSource.DataSet.IsEmpty) then
  begin
    if (MessageDlg('هل انت متاكد من الحذف', mtCustom, [mbYes, mbNo], 0)) = mrYes
    then
    begin
      DBGrid1.DataSource.DataSet.Delete;
      ShowMessage('تم الحذف');
    end
  end;
end;

procedure Tfrm_sizes.EditCarClick(Sender: TObject);
begin
  Databse.tblSize.edit;
end;

procedure Tfrm_sizes.NewCarClick(Sender: TObject);
begin
  Databse.tblSize.Append;
end;

end.
