unit HomeScreen;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, cxLabel, Vcl.StdCtrls,
  cxButtons, cxDBLabel, dxGDIPlusClasses, cxImage, cxCheckBox, dxToggleSwitch,
  cxGroupBox, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;

type
  TForm1 = class(TForm)
    HeaderPnl: TcxGroupBox;
    MenuToggleBtn: TdxToggleSwitch;
    cxGroupBox1: TcxGroupBox;
    RefreshBtn: TcxButton;
    MenuPnl: TcxGroupBox;
    TasksBtn: TcxButton;
    Databtn: TcxButton;
    ReportsBtn: TcxButton;
    OptionsBtn: TcxButton;
    HomeBtn: TcxButton;
    StatisticsBtn: TcxButton;
    MainPnl: TcxGroupBox;
    Label1: TLabel;
    procedure HomeBtnClick(Sender: TObject);
    procedure DatabtnClick(Sender: TObject);
    procedure TasksBtnClick(Sender: TObject);
    procedure StatisticsBtnClick(Sender: TObject);
    procedure ReportsBtnClick(Sender: TObject);
    procedure OptionsBtnClick(Sender: TObject);
    procedure MenuToggleBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses Theme, DataEntry, OperationsScreen;

procedure TForm1.DatabtnClick(Sender: TObject);
begin
MainPnl.Caption := (Sender as TCXButton).Caption;
DataEntryFrm.Parent := MainPnl;
DataEntryFrm.Show;
end;

procedure TForm1.HomeBtnClick(Sender: TObject);
begin
MainPnl.Caption := (Sender as TCXButton).Caption;
end;

procedure TForm1.MenuToggleBtnClick(Sender: TObject);
begin
 if menupnl.Visible then
  menupnl.Hide else menupnl.show;

end;

procedure TForm1.OptionsBtnClick(Sender: TObject);
begin
MainPnl.Caption := (Sender as TCXButton).Caption;
end;

procedure TForm1.ReportsBtnClick(Sender: TObject);
begin
MainPnl.Caption := (Sender as TCXButton).Caption;
end;

procedure TForm1.StatisticsBtnClick(Sender: TObject);
begin
MainPnl.Caption := (Sender as TCXButton).Caption;
end;

procedure TForm1.TasksBtnClick(Sender: TObject);
begin
MainPnl.Caption := (Sender as TCXButton).Caption;
OperationsFrm.Parent := MainPnl;
OperationsFrm.Show;
end;

end.
