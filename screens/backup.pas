unit backup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, cxGroupBox, Vcl.Menus, Vcl.StdCtrls, cxButtons;

type
  Tfrmbackup = class(TForm)
    cxGroupBox1: TcxGroupBox;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    procedure cxButton1Click(Sender: TObject);

    procedure BackupDB(const DBName: String);
    procedure RestoreDB(const DBName: String);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmbackup: Tfrmbackup;

implementation

{$R *.dfm}

uses Database;

var
  scBackUpPath: string;

const
  scBackupExtn = '.Bak';

procedure Tfrmbackup.cxButton1Click(Sender: TObject);
begin
  if SaveDialog1.Execute() then
  begin
    scBackUpPath := SaveDialog1.FileName;
    BackupDB('Taffsil');
  end;

end;

procedure Tfrmbackup.cxButton2Click(Sender: TObject);
begin
  if SaveDialog1.Execute() then
  begin
    scBackUpPath := SaveDialog1.FileName;
  RestoreDB('Taffsil');
  end;

end;

procedure Tfrmbackup.BackupDB(const DBName: String);
var
  FileName, Sql: String;
begin
  FileName := scBackUpPath + DBName + scBackupExtn;
  Sql := 'backup database %s to disk = ''%s''';
  Sql := Format(Sql, [DBName, FileName]);
  // Log('Backing up ' + DBName + ' using SQL: ' + Sql);
  try
    Screen.Cursor := crHourGlass;
    Update;
    Databse.db.ExecSQL(Sql);
    // Log('Done');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure Tfrmbackup.RestoreDB(const DBName: String);
var
  FileName, Sql: String;
begin
  FileName := scBackUpPath + DBName + scBackupExtn;
  if FileExists(FileName) then
  begin
    // Note:  beware the 'with replace' in the following
    Sql := 'restore database %s from disk = ''%s'' with replace';
    Sql := Format(Sql, [DBName, FileName]);
    // Log('Restoring ' + DBName + ' using SQL: ' + Sql);
    try
      Screen.Cursor := crHourGlass;
      Update;
      Databse.db.ExecSQL(Sql);
      // Log('Done');
    finally
      Screen.Cursor := crDefault;
    end;
  end
  else
    // Log('Backup file ' + FileName + ' not found!');
end;

end.
