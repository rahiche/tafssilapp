unit TafssilScreen;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, cxControls,
  cxContainer, cxEdit, cxMaskEdit, cxDropDownEdit, cxTextEdit, cxDBEdit,
  cxGroupBox, cxSpinEdit, cxCalendar, cxNavigator, cxDBNavigator, cxImage,
  cxRadioGroup, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Data.DB,
  Vcl.Grids, Vcl.DBGrids, Vcl.DBCtrls;

type
  TtafssilFrm = class(TForm)
    HeaderPnl: TPanel;
    Label1: TLabel;
    NewCar: TcxButton;
    DeleteCar: TcxButton;
    EditCar: TcxButton;
    cxButton7: TcxButton;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxGroupBox1: TcxGroupBox;
    Label2: TLabel;
    cxDBTextEdit1: TcxDBTextEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    cxComboBox1: TcxComboBox;
    cxDBTextEdit2: TcxDBTextEdit;
    cxDBTextEdit3: TcxDBTextEdit;
    cxDBTextEdit4: TcxDBTextEdit;
    cxDBTextEdit5: TcxDBTextEdit;
    cxDBTextEdit6: TcxDBTextEdit;
    pnlSizes: TGridPanel;
    Panel1: TPanel;
    Label9: TLabel;
    cxDBSpinEdit1: TcxDBSpinEdit;
    Panel2: TPanel;
    Label10: TLabel;
    cxDBSpinEdit2: TcxDBSpinEdit;
    Panel3: TPanel;
    Label11: TLabel;
    cxDBSpinEdit3: TcxDBSpinEdit;
    Panel4: TPanel;
    Label12: TLabel;
    cxDBSpinEdit4: TcxDBSpinEdit;
    Panel5: TPanel;
    Label13: TLabel;
    cxDBSpinEdit5: TcxDBSpinEdit;
    Panel6: TPanel;
    Label14: TLabel;
    cxDBSpinEdit6: TcxDBSpinEdit;
    Panel7: TPanel;
    Label15: TLabel;
    cxDBSpinEdit7: TcxDBSpinEdit;
    Panel8: TPanel;
    Label16: TLabel;
    cxDBSpinEdit8: TcxDBSpinEdit;
    Panel9: TPanel;
    Label17: TLabel;
    cxDBSpinEdit9: TcxDBSpinEdit;
    Panel10: TPanel;
    Label18: TLabel;
    cxDBSpinEdit10: TcxDBSpinEdit;
    cxGroupBox2: TcxGroupBox;
    Label19: TLabel;
    cxDBTextEdit7: TcxDBTextEdit;
    Label20: TLabel;
    cxDBTextEdit8: TcxDBTextEdit;
    Label21: TLabel;
    cxDBTextEdit9: TcxDBTextEdit;
    Label22: TLabel;
    cxDBTextEdit10: TcxDBTextEdit;
    Label23: TLabel;
    cxDBTextEdit11: TcxDBTextEdit;
    Label24: TLabel;
    cxDBTextEdit12: TcxDBTextEdit;
    Label25: TLabel;
    cxDBTextEdit13: TcxDBTextEdit;
    Label26: TLabel;
    cxComboBox2: TcxComboBox;
    Label27: TLabel;
    cxDBDateEdit1: TcxDBDateEdit;
    cxDBDateEdit2: TcxDBDateEdit;
    Label28: TLabel;
    cxGroupBox14: TcxGroupBox;
    Panel15: TPanel;
    GridPanel1: TGridPanel;
    cxGroupBox3: TcxGroupBox;
    cxDBImage1: TcxDBImage;
    cxGroupBox4: TcxGroupBox;
    cxDBTextEdit15: TcxDBTextEdit;
    cxDBNavigator1: TcxDBNavigator;
    cxDBNavigator2: TcxDBNavigator;
    cxGroupBox5: TcxGroupBox;
    cxDBImage2: TcxDBImage;
    cxGroupBox6: TcxGroupBox;
    cxDBTextEdit14: TcxDBTextEdit;
    cxDBNavigator3: TcxDBNavigator;
    cxDBNavigator4: TcxDBNavigator;
    cxGroupBox7: TcxGroupBox;
    cxDBImage3: TcxDBImage;
    cxGroupBox8: TcxGroupBox;
    cxDBTextEdit16: TcxDBTextEdit;
    cxDBNavigator5: TcxDBNavigator;
    cxDBNavigator6: TcxDBNavigator;
    cxGroupBox9: TcxGroupBox;
    cxDBImage4: TcxDBImage;
    cxGroupBox10: TcxGroupBox;
    cxDBTextEdit17: TcxDBTextEdit;
    cxDBNavigator7: TcxDBNavigator;
    cxDBNavigator8: TcxDBNavigator;
    cxGroupBox11: TcxGroupBox;
    cxDBImage5: TcxDBImage;
    cxGroupBox12: TcxGroupBox;
    cxDBTextEdit18: TcxDBTextEdit;
    cxDBNavigator9: TcxDBNavigator;
    cxDBNavigator10: TcxDBNavigator;
    Panel11: TPanel;
    cxDBRadioGroup2: TcxDBRadioGroup;
    cxGroupBox13: TcxGroupBox;
    GridPanel2: TGridPanel;
    Panel12: TPanel;
    Label29: TLabel;
    cxDBLookupComboBox2: TcxDBLookupComboBox;
    Panel13: TPanel;
    Label30: TLabel;
    cxDBLookupComboBox4: TcxDBLookupComboBox;
    Panel14: TPanel;
    Label31: TLabel;
    cxDBTextEdit19: TcxDBTextEdit;
    Panel16: TPanel;
    Label34: TLabel;
    cxDBLookupComboBox5: TcxDBLookupComboBox;
    Panel17: TPanel;
    Label35: TLabel;
    cxDBLookupComboBox3: TcxDBLookupComboBox;
    Panel18: TPanel;
    Label36: TLabel;
    cxDBSpinEdit11: TcxDBSpinEdit;
    cxDBRadioGroup1: TcxDBRadioGroup;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  tafssilFrm: TtafssilFrm;

implementation

{$R *.dfm}

uses Database, OperationsScreen;

procedure TtafssilFrm.cxButton7Click(Sender: TObject);
begin
  close;
  with OperationsFrm do
  begin
    DisplayPnl.Align := alBottom;
    DisplayPnl.hide;
    GridPanel1.show;
  end;
end;

procedure TtafssilFrm.cxComboBox1PropertiesChange(Sender: TObject);
begin
  if cxComboBox1.SelectedItem = 0 then
  begin
    pnlSizes.Show;
  end
  else
    pnlSizes.Hide;
end;

end.
