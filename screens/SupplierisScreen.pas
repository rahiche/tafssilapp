﻿unit SupplierisScreen;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog, Data.DB,
  cxDBData, Vcl.Menus, cxContainer, cxTextEdit, Vcl.StdCtrls, cxButtons,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ExtCtrls, cxGroupBox,
  Vcl.Mask, Vcl.DBCtrls;

type
  Tfrm_suppliers = class(TForm)
    DisplayPnl: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    HeaderPnl: TPanel;
    Label1: TLabel;
    NewCar: TcxButton;
    DeleteCar: TcxButton;
    EditCar: TcxButton;
    cxButton7: TcxButton;
    DataPnl: TcxGroupBox;
    cxGroupBox2: TcxGroupBox;
    yesBtn: TcxButton;
    noBtn: TcxButton;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1address: TcxGridDBColumn;
    cxGrid1DBTableView1phone_1: TcxGridDBColumn;
    cxGrid1DBTableView1phone_2: TcxGridDBColumn;
    cxGrid1DBTableView1email: TcxGridDBColumn;
    procedure cxButton7Click(Sender: TObject);
    procedure DeleteCarClick(Sender: TObject);
    procedure EditCarClick(Sender: TObject);
    procedure NewCarClick(Sender: TObject);
    procedure yesBtnClick(Sender: TObject);
    procedure noBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_suppliers: Tfrm_suppliers;

implementation

{$R *.dfm}

uses DataEntry, Database;

procedure Tfrm_suppliers.cxButton7Click(Sender: TObject);
begin
  close;
  with DataEntryFrm do
  begin
    Databse.tblSupplier.close();
    DisplayPnl.Align := alBottom;
    DisplayPnl.hide;
    GridPanel1.show;
  end;
end;

procedure Tfrm_suppliers.DeleteCarClick(Sender: TObject);
begin
  if (not cxGrid1DBTableView1.DataController.DataSource.DataSet.IsEmpty) then
  begin
    if (MessageDlg('هل انت متاكد من الحذف', mtCustom, [mbYes, mbNo], 0)) = mrYes
    then
    begin
      cxGrid1DBTableView1.DataController.DataSource.DataSet.Delete;
      ShowMessage('تم الحذف');
    end
  end;
end;

procedure Tfrm_suppliers.EditCarClick(Sender: TObject);
begin
  if (not DataPnl.Showing) then
  begin
    Databse.tblSupplier.edit;
    DataPnl.show;
    DataPnl.SetFocus;
    HeaderPnl.Enabled := false;
  end;
end;

procedure Tfrm_suppliers.NewCarClick(Sender: TObject);
begin
  if (not DataPnl.Showing) then
  begin
    Databse.tblSupplier.Insert;
    DataPnl.show;
    DataPnl.SetFocus;
    HeaderPnl.Enabled := false;
  end;
end;

procedure Tfrm_suppliers.noBtnClick(Sender: TObject);
begin
  if (DataPnl.Showing) then
  begin
    Databse.tblSupplier.Cancel;
    DataPnl.hide;
    HeaderPnl.Enabled := true;
    HeaderPnl.SetFocus;

  end;
end;

procedure Tfrm_suppliers.yesBtnClick(Sender: TObject);
begin
  if (DataPnl.Showing) then
  begin
    Databse.tblSupplier.post;
    DataPnl.hide;
    HeaderPnl.Enabled := true;
    HeaderPnl.SetFocus;

    ShowMessage('تم الحفظ بنجاح');
  end;
end;

end.
