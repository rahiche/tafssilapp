object Databse: TDatabse
  OldCreateOrder = False
  Height = 605
  Width = 933
  object db: TFDConnection
    Params.Strings = (
      'Database=Taffsil'
      'Server=DESKTOP-6KLOFOI\SQLEXPRESS'
      'OSAuthent=Yes'
      'DriverID=MSSQL')
    Connected = True
    LoginPrompt = False
    Left = 328
    Top = 16
  end
  object dslab: TDataSource
    DataSet = tblLab
    Left = 48
    Top = 144
  end
  object tblLab: TFDTable
    IndexFieldNames = 'lab_id'
    Connection = db
    UpdateOptions.UpdateTableName = 'Taffsil.dbo.Lab'
    TableName = 'Taffsil.dbo.Lab'
    Left = 48
    Top = 88
    object tblLablab_id: TIntegerField
      Alignment = taCenter
      FieldName = 'lab_id'
      Origin = 'lab_id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object tblLabname_ar: TWideStringField
      Alignment = taCenter
      FieldName = 'name_ar'
      Origin = 'name_ar'
      Size = 50
    end
    object tblLabname_en: TWideStringField
      Alignment = taCenter
      FieldName = 'name_en'
      Origin = 'name_en'
      Size = 50
    end
    object tblLabcomments: TWideStringField
      Alignment = taCenter
      FieldName = 'comments'
      Origin = 'comments'
      Size = 50
    end
  end
  object dsTailor: TDataSource
    DataSet = tblTailor
    Left = 102
    Top = 144
  end
  object tblTailor: TFDTable
    IndexFieldNames = 'tailor_id'
    Connection = db
    UpdateOptions.UpdateTableName = 'Taffsil.dbo.Tailor'
    TableName = 'Taffsil.dbo.Tailor'
    Left = 102
    Top = 88
    object tblTailortailor_id: TFDAutoIncField
      Alignment = taCenter
      FieldName = 'tailor_id'
      Origin = 'tailor_id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object tblTailorname_ar: TWideStringField
      Alignment = taCenter
      FieldName = 'name_ar'
      Origin = 'name_ar'
      Size = 50
    end
    object tblTailorname_en: TWideStringField
      Alignment = taCenter
      FieldName = 'name_en'
      Origin = 'name_en'
      Size = 50
    end
    object tblTailorphone_1: TWideStringField
      Alignment = taCenter
      FieldName = 'phone_1'
      Origin = 'phone_1'
      Size = 50
    end
    object tblTailorphone_2: TWideStringField
      Alignment = taCenter
      FieldName = 'phone_2'
      Origin = 'phone_2'
      Size = 50
    end
    object tblTailoremail: TWideStringField
      Alignment = taCenter
      FieldName = 'email'
      Origin = 'email'
      Size = 50
    end
    object tblTailorcomments: TWideStringField
      Alignment = taCenter
      FieldName = 'comments'
      Origin = 'comments'
      Size = 50
    end
  end
  object dsCategoryGroup: TDataSource
    DataSet = tblCategoryGroup
    Left = 176
    Top = 144
  end
  object tblCategoryGroup: TFDTable
    IndexFieldNames = 'category_group_id'
    Connection = db
    UpdateOptions.UpdateTableName = 'Taffsil.dbo.CategoryGroup'
    TableName = 'Taffsil.dbo.CategoryGroup'
    Left = 176
    Top = 88
    object tblCategoryGroupcategory_group_id: TFDAutoIncField
      FieldName = 'category_group_id'
      Origin = 'category_group_id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object tblCategoryGroupname_ar: TWideStringField
      FieldName = 'name_ar'
      Origin = 'name_ar'
      Size = 50
    end
    object tblCategoryGroupname_en: TWideStringField
      FieldName = 'name_en'
      Origin = 'name_en'
      Size = 50
    end
    object tblCategoryGroupserial_number: TWideStringField
      FieldName = 'serial_number'
      Origin = 'serial_number'
      Size = 50
    end
  end
  object dsCategory: TDataSource
    DataSet = tblCategory
    Left = 256
    Top = 144
  end
  object tblCategory: TFDTable
    IndexFieldNames = 'category_id'
    Connection = db
    UpdateOptions.UpdateTableName = 'Category'
    TableName = 'Category'
    Left = 256
    Top = 88
    object tblCategorycategory_id: TFDAutoIncField
      Alignment = taCenter
      FieldName = 'category_id'
      Origin = 'category_id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object tblCategorycategory_groupe_id: TIntegerField
      Alignment = taCenter
      FieldName = 'category_groupe_id'
      Origin = 'category_groupe_id'
    end
    object tblCategoryname_ar: TWideStringField
      Alignment = taCenter
      FieldName = 'name_ar'
      Origin = 'name_ar'
      Size = 50
    end
    object tblCategoryname_en: TWideStringField
      Alignment = taCenter
      FieldName = 'name_en'
      Origin = 'name_en'
      Size = 50
    end
    object tblCategorysell_unit: TWideStringField
      Alignment = taCenter
      FieldName = 'sell_unit'
      Origin = 'sell_unit'
      Size = 50
    end
    object tblCategoryinitial_balance_yard: TFloatField
      Alignment = taCenter
      FieldName = 'initial_balance_yard'
      Origin = 'initial_balance_yard'
    end
    object tblCategorysell_price: TFloatField
      Alignment = taCenter
      FieldName = 'sell_price'
      Origin = 'sell_price'
    end
    object tblCategorywork_price: TFloatField
      Alignment = taCenter
      FieldName = 'work_price'
      Origin = 'work_price'
    end
    object tblCategoryyard_price: TFloatField
      Alignment = taCenter
      FieldName = 'yard_price'
      Origin = 'yard_price'
    end
  end
  object dsCustomer: TDataSource
    DataSet = tblCustomer
    Left = 320
    Top = 144
  end
  object tblCustomer: TFDTable
    IndexFieldNames = 'customer_id'
    Connection = db
    UpdateOptions.UpdateTableName = 'Taffsil.dbo.Customer'
    TableName = 'Taffsil.dbo.Customer'
    Left = 320
    Top = 88
    object tblCustomercustomer_id: TFDAutoIncField
      Alignment = taCenter
      FieldName = 'customer_id'
      Origin = 'customer_id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object tblCustomername: TWideStringField
      Alignment = taCenter
      FieldName = 'name'
      Origin = 'name'
      Size = 50
    end
    object tblCustomerphone_1: TWideStringField
      Alignment = taCenter
      FieldName = 'phone_1'
      Origin = 'phone_1'
      Size = 50
    end
    object tblCustomerphone_2: TWideStringField
      Alignment = taCenter
      FieldName = 'phone_2'
      Origin = 'phone_2'
      Size = 50
    end
    object tblCustomeremail: TWideStringField
      Alignment = taCenter
      FieldName = 'email'
      Origin = 'email'
      Size = 50
    end
    object tblCustomercomments: TWideStringField
      Alignment = taCenter
      FieldName = 'comments'
      Origin = 'comments'
      Size = 50
    end
    object tblCustomerdata: TIntegerField
      Alignment = taCenter
      FieldName = 'data'
      Origin = 'data'
    end
    object tblCustomersize1: TFloatField
      Alignment = taCenter
      FieldName = 'size1'
      Origin = 'size1'
    end
    object tblCustomersize10: TFloatField
      Alignment = taCenter
      FieldName = 'size10'
      Origin = 'size10'
    end
    object tblCustomersize9: TFloatField
      Alignment = taCenter
      FieldName = 'size9'
      Origin = 'size9'
    end
    object tblCustomersize8: TFloatField
      Alignment = taCenter
      FieldName = 'size8'
      Origin = 'size8'
    end
    object tblCustomersize7: TFloatField
      Alignment = taCenter
      FieldName = 'size7'
      Origin = 'size7'
    end
    object tblCustomersize6: TFloatField
      Alignment = taCenter
      FieldName = 'size6'
      Origin = 'size6'
    end
    object tblCustomersize5: TFloatField
      Alignment = taCenter
      FieldName = 'size5'
      Origin = 'size5'
    end
    object tblCustomersize4: TFloatField
      Alignment = taCenter
      FieldName = 'size4'
      Origin = 'size4'
    end
    object tblCustomersize3: TFloatField
      Alignment = taCenter
      FieldName = 'size3'
      Origin = 'size3'
    end
    object tblCustomersize2: TFloatField
      Alignment = taCenter
      FieldName = 'size2'
      Origin = 'size2'
    end
  end
  object dsSupplier: TDataSource
    DataSet = tblSupplier
    Left = 384
    Top = 144
  end
  object tblSupplier: TFDTable
    IndexFieldNames = 'supplier_id'
    Connection = db
    UpdateOptions.UpdateTableName = 'Supplier'
    TableName = 'Supplier'
    Left = 384
    Top = 88
    object tblSuppliersupplier_id: TFDAutoIncField
      Alignment = taCenter
      FieldName = 'supplier_id'
      Origin = 'supplier_id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object tblSuppliername: TWideStringField
      Alignment = taCenter
      FieldName = 'name'
      Origin = 'name'
      Size = 50
    end
    object tblSupplieraddress: TWideStringField
      Alignment = taCenter
      FieldName = 'address'
      Origin = 'address'
      Size = 50
    end
    object tblSupplierphone_1: TWideStringField
      Alignment = taCenter
      FieldName = 'phone_1'
      Origin = 'phone_1'
      Size = 50
    end
    object tblSupplierphone_2: TWideStringField
      Alignment = taCenter
      FieldName = 'phone_2'
      Origin = 'phone_2'
      Size = 50
    end
    object tblSupplieremail: TWideStringField
      Alignment = taCenter
      FieldName = 'email'
      Origin = 'email'
      Size = 50
    end
  end
  object dsSize: TDataSource
    DataSet = tblSize
    Left = 440
    Top = 144
  end
  object tblSize: TFDTable
    IndexFieldNames = 'id'
    Connection = db
    UpdateOptions.UpdateTableName = 'Taffsil.dbo.Size'
    TableName = 'Taffsil.dbo.Size'
    Left = 440
    Top = 88
    object tblSizeid: TFDAutoIncField
      Alignment = taCenter
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object tblSizesize1: TFloatField
      Alignment = taCenter
      FieldName = 'size1'
      Origin = 'size1'
    end
    object tblSizesize10: TFloatField
      Alignment = taCenter
      FieldName = 'size10'
      Origin = 'size10'
    end
    object tblSizesize9: TFloatField
      Alignment = taCenter
      FieldName = 'size9'
      Origin = 'size9'
    end
    object tblSizesize8: TFloatField
      Alignment = taCenter
      FieldName = 'size8'
      Origin = 'size8'
    end
    object tblSizesize7: TFloatField
      Alignment = taCenter
      FieldName = 'size7'
      Origin = 'size7'
    end
    object tblSizesize6: TFloatField
      Alignment = taCenter
      FieldName = 'size6'
      Origin = 'size6'
    end
    object tblSizesize5: TFloatField
      Alignment = taCenter
      FieldName = 'size5'
      Origin = 'size5'
    end
    object tblSizesize4: TFloatField
      Alignment = taCenter
      FieldName = 'size4'
      Origin = 'size4'
    end
    object tblSizesize3: TFloatField
      Alignment = taCenter
      FieldName = 'size3'
      Origin = 'size3'
    end
    object tblSizesize2: TFloatField
      Alignment = taCenter
      FieldName = 'size2'
      Origin = 'size2'
    end
    object tblSizename_ar: TWideStringField
      Alignment = taCenter
      FieldName = 'name_ar'
      Origin = 'name_ar'
      Size = 50
    end
    object tblSizename_en: TWideStringField
      Alignment = taCenter
      FieldName = 'name_en'
      Origin = 'name_en'
      Size = 50
    end
    object tblSizecomment: TWideStringField
      Alignment = taCenter
      FieldName = 'comment'
      Origin = 'comment'
      Size = 50
    end
  end
  object dsUser: TDataSource
    DataSet = tblUser
    Left = 848
    Top = 144
  end
  object tblUser: TFDTable
    Connection = db
    UpdateOptions.UpdateTableName = '[User]'
    TableName = '[User]'
    Left = 848
    Top = 88
  end
  object dsUserPermission: TDataSource
    DataSet = tblUserPermission
    Left = 848
    Top = 256
  end
  object tblUserPermission: TFDTable
    Connection = db
    UpdateOptions.UpdateTableName = 'Taffsil.dbo.UserPermission'
    TableName = 'Taffsil.dbo.UserPermission'
    Left = 848
    Top = 200
  end
  object DataSource1: TDataSource
    DataSet = iamges1
    Left = 40
    Top = 552
  end
  object iamges1: TFDTable
    Active = True
    IndexFieldNames = 'id'
    Connection = db
    UpdateOptions.UpdateTableName = 'Taffsil.dbo.images1'
    TableName = 'Taffsil.dbo.images1'
    Left = 40
    Top = 496
    object iamges1id: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object iamges1image: TBlobField
      FieldName = 'image'
      Origin = 'image'
    end
    object iamges1comment: TWideStringField
      FieldName = 'comment'
      Origin = 'comment'
      FixedChar = True
      Size = 10
    end
  end
  object DataSource2: TDataSource
    DataSet = images2
    Left = 104
    Top = 552
  end
  object images2: TFDTable
    Active = True
    IndexFieldNames = 'id'
    Connection = db
    UpdateOptions.UpdateTableName = 'images2'
    TableName = 'images2'
    Left = 104
    Top = 496
    object images2id: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object images2image: TBlobField
      FieldName = 'image'
      Origin = 'image'
    end
    object images2comment: TWideStringField
      FieldName = 'comment'
      Origin = 'comment'
      FixedChar = True
      Size = 10
    end
  end
  object DataSource3: TDataSource
    DataSet = images3
    Left = 176
    Top = 552
  end
  object images3: TFDTable
    Active = True
    IndexFieldNames = 'id'
    Connection = db
    UpdateOptions.UpdateTableName = 'Taffsil.dbo.images3'
    TableName = 'Taffsil.dbo.images3'
    Left = 176
    Top = 496
    object images3id: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object images3image: TBlobField
      FieldName = 'image'
      Origin = 'image'
    end
    object images3comment: TWideStringField
      FieldName = 'comment'
      Origin = 'comment'
      FixedChar = True
      Size = 10
    end
  end
  object DataSource4: TDataSource
    DataSet = images4
    Left = 232
    Top = 552
  end
  object images4: TFDTable
    Active = True
    IndexFieldNames = 'id'
    Connection = db
    UpdateOptions.UpdateTableName = 'Taffsil.dbo.images4'
    TableName = 'Taffsil.dbo.images4'
    Left = 232
    Top = 496
    object images4id: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object images4image: TBlobField
      FieldName = 'image'
      Origin = 'image'
    end
    object images4comment: TWideStringField
      FieldName = 'comment'
      Origin = 'comment'
      FixedChar = True
      Size = 10
    end
  end
  object DataSource5: TDataSource
    DataSet = images5
    Left = 296
    Top = 552
  end
  object images5: TFDTable
    Active = True
    IndexFieldNames = 'id'
    Connection = db
    UpdateOptions.UpdateTableName = 'Taffsil.dbo.images5'
    TableName = 'Taffsil.dbo.images5'
    Left = 296
    Top = 496
    object images5id: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object images5image: TBlobField
      FieldName = 'image'
      Origin = 'image'
    end
    object images5comment: TWideStringField
      FieldName = 'comment'
      Origin = 'comment'
      FixedChar = True
      Size = 10
    end
  end
end
