unit Database;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MSSQL,
  FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet;

type
  TDatabse = class(TDataModule)
    db: TFDConnection;
    dslab: TDataSource;
    tblLab: TFDTable;
    tblLablab_id: TIntegerField;
    tblLabname_ar: TWideStringField;
    tblLabname_en: TWideStringField;
    tblLabcomments: TWideStringField;
    dsTailor: TDataSource;
    tblTailor: TFDTable;
    dsCategoryGroup: TDataSource;
    tblCategoryGroup: TFDTable;
    dsCategory: TDataSource;
    tblCategory: TFDTable;
    dsCustomer: TDataSource;
    tblCustomer: TFDTable;
    dsSupplier: TDataSource;
    tblSupplier: TFDTable;
    dsSize: TDataSource;
    tblSize: TFDTable;
    dsUser: TDataSource;
    tblUser: TFDTable;
    dsUserPermission: TDataSource;
    tblUserPermission: TFDTable;
    tblTailortailor_id: TFDAutoIncField;
    tblTailorname_ar: TWideStringField;
    tblTailorname_en: TWideStringField;
    tblTailorphone_1: TWideStringField;
    tblTailorphone_2: TWideStringField;
    tblTailoremail: TWideStringField;
    tblTailorcomments: TWideStringField;
    tblCategoryGroupcategory_group_id: TFDAutoIncField;
    tblCategoryGroupname_ar: TWideStringField;
    tblCategoryGroupname_en: TWideStringField;
    tblCategoryGroupserial_number: TWideStringField;
    tblCategorycategory_id: TFDAutoIncField;
    tblCategorycategory_groupe_id: TIntegerField;
    tblCategoryname_ar: TWideStringField;
    tblCategoryname_en: TWideStringField;
    tblCategorysell_unit: TWideStringField;
    tblCategoryinitial_balance_yard: TFloatField;
    tblCategorysell_price: TFloatField;
    tblCategorywork_price: TFloatField;
    tblCategoryyard_price: TFloatField;
    tblCustomercustomer_id: TFDAutoIncField;
    tblCustomername: TWideStringField;
    tblCustomerphone_1: TWideStringField;
    tblCustomerphone_2: TWideStringField;
    tblCustomeremail: TWideStringField;
    tblCustomercomments: TWideStringField;
    tblCustomerdata: TIntegerField;
    tblCustomersize1: TFloatField;
    tblCustomersize10: TFloatField;
    tblCustomersize9: TFloatField;
    tblCustomersize8: TFloatField;
    tblCustomersize7: TFloatField;
    tblCustomersize6: TFloatField;
    tblCustomersize5: TFloatField;
    tblCustomersize4: TFloatField;
    tblCustomersize3: TFloatField;
    tblCustomersize2: TFloatField;
    tblSuppliersupplier_id: TFDAutoIncField;
    tblSuppliername: TWideStringField;
    tblSupplieraddress: TWideStringField;
    tblSupplierphone_1: TWideStringField;
    tblSupplierphone_2: TWideStringField;
    tblSupplieremail: TWideStringField;
    tblSizeid: TFDAutoIncField;
    tblSizesize1: TFloatField;
    tblSizesize10: TFloatField;
    tblSizesize9: TFloatField;
    tblSizesize8: TFloatField;
    tblSizesize7: TFloatField;
    tblSizesize6: TFloatField;
    tblSizesize5: TFloatField;
    tblSizesize4: TFloatField;
    tblSizesize3: TFloatField;
    tblSizesize2: TFloatField;
    tblSizename_ar: TWideStringField;
    tblSizename_en: TWideStringField;
    tblSizecomment: TWideStringField;
    DataSource1: TDataSource;
    iamges1: TFDTable;
    iamges1id: TFDAutoIncField;
    iamges1image: TBlobField;
    iamges1comment: TWideStringField;
    DataSource2: TDataSource;
    images2: TFDTable;
    DataSource3: TDataSource;
    images3: TFDTable;
    DataSource4: TDataSource;
    images4: TFDTable;
    DataSource5: TDataSource;
    images5: TFDTable;
    images2id: TFDAutoIncField;
    images2image: TBlobField;
    images2comment: TWideStringField;
    images3id: TFDAutoIncField;
    images3image: TBlobField;
    images3comment: TWideStringField;
    images4id: TFDAutoIncField;
    images4image: TBlobField;
    images4comment: TWideStringField;
    images5id: TFDAutoIncField;
    images5image: TBlobField;
    images5comment: TWideStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Databse: TDatabse;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
